import React from 'react';
import {fireEvent, render} from "@testing-library/react";
import {Harry} from "../1_snapshots/components/Harry";

// more https://testing-library.com/docs/react-testing-library/intro

// important differences in queries https://testing-library.com/docs/dom-testing-library/api-queries

test('ReactTestingLibrary', () => {

	const { container, getByAltText, getAllByText, getByText, getByTitle, getByTestId } = render(
		<Harry/>,
	);

	expect(container.firstChild).toBeNull();

	expect(getByAltText("Juventus")).toBeTruthy();
	expect(getByText("Juventus")).toBeTruthy();
	expect(getByTitle("Jisty postup")).toBeTruthy();

	expect(getAllByText("Dominic Thiem")).toHaveLength(2);
	expect(getByText("/")).toBeTruthy();
	expect(getAllByText("Dennis Novak")).toHaveLength(2);

	expect(getByTitle("Serie A (Itálie)").getAttribute("href")).toBe("/fotbal/italie/serie-a/");

	expect(container.getElementsByClassName("profileTable__row--last").item(0)).not.toBeNull();

	fireEvent.click(getByText("TRANS_KEY"));

	expect(container.firstElementChild?.id).toBe("last-matches");

	const closeButton = container.querySelectorAll(".close");
	expect(closeButton.length).toBe(1);

	const homeBar = getByTestId("homeBar");
	expect(homeBar.style.width).toBe("50%");
	expect(homeBar.classList.contains("someClass")).toBeTruthy();
});
