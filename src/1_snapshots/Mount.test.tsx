import React from 'react';
import {Harry} from "./components/Harry";
import {mount} from "enzyme";
import {HarryClass} from "./components/HarryClass";

// https://github.com/enzymejs/enzyme/blob/master/docs/api/mount.md

// Full DOM rendering is ideal for use cases where you have components that may interact with DOM APIs or need to
// test components that are wrapped in higher order components.

// Full DOM rendering requires that a full DOM API be available at the global scope. This means that it must
// be run in an environment that at least “looks like” a browser environment. If you do not want to run your
// tests inside of a browser, the recommended approach to using mount is to depend on a library called jsdom
// which is essentially a headless browser implemented completely in JS.

describe('mount', () => {
	test('mount basic', () => {
		const wrapper = mount(
			<Harry/>,
		);

		expect(wrapper.find('.Harry')).toHaveLength(1);
		expect(wrapper.find('.Vojta')).toHaveLength(1);

		expect(wrapper.html()).toMatchSnapshot(); // https://enzymejs.github.io/enzyme/docs/api/ShallowWrapper/html.html
	});

	test('allows us to set props', () => {
		const wrapper = mount(<Harry age={1}/>);
		expect(wrapper.props().age).toBe(1);
		wrapper.setProps({age: 2});
		expect(wrapper.props().age).toBe(2);
	});

	test('calls componentDidMount', () => {
		jest.spyOn(HarryClass.prototype, 'componentDidMount');
		mount(<HarryClass/>);
		expect(HarryClass.prototype.componentDidMount).toBeCalledTimes(1);
	});
});

