import React from 'react';
import {Vojta} from "./Vojta";

interface HarryProps {
	age?: number;
	onClick?: () => void;
}

export class HarryClass extends React.PureComponent<HarryProps> {

	componentDidMount(): void {
		console.log("Harry smrdi");
	}

	render() {
		return (
			<div className={"Harry"} onClick={this.props.onClick}>
				<Vojta/>
			</div>
		);
	}
}
