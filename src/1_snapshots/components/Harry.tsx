import React from 'react';
import {Vojta} from "./Vojta";

interface HarryProps {
	age?: number;
	onClick?: () => void;
}

export function Harry({ age, onClick }: HarryProps) {
	return (
		<div className={"Harry"} onClick={onClick}>
			<Vojta/>
		</div>
	);
}
