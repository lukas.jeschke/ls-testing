import React from 'react';
import {shallow} from "enzyme";
import {Harry} from "./components/Harry";

// https://github.com/enzymejs/enzyme/blob/master/docs/api/shallow.md

// Shallow rendering is useful to constrain yourself to testing a component as a unit, and to ensure that your
// tests aren't indirectly asserting on behavior of child components.

//  Shallow method is used to render the single component that we are testing. It does not render child components.
// 	Simple shallow calls the constructor, render, componentDidMount (in Enzyme version 3) methods.
// 	shallow + setProps call componentWillReceiveProps, shouldComponentUpdate, componentWillUpdate, render, componentDidUpdate (in Enzyme version 3) methods.
// 	shallow + unmount call componentWillUnmount method.

// As of Enzyme v3, the shallow API does call React lifecycle methods such as componentDidMount and componentDidUpdate.
// You can read more about this in https://github.com/enzymejs/enzyme/blob/master/docs/guides/migration-from-2-to-3.md#lifecycle-methods.

describe('shallow', () => {
	test('shallow basic', () => {
		const wrapper = shallow(
			<Harry/>,
		);

		expect(wrapper.find('.Harry')).toHaveLength(1);
		// expect(wrapper.find('.Vojta')).toHaveLength(1);

		expect(wrapper.html()).toMatchSnapshot(); // https://enzymejs.github.io/enzyme/docs/api/ShallowWrapper/html.html
	});

	test('simulates click events', () => {
		const onClick = jest.fn();
		const wrapper = shallow(
			<Harry onClick={onClick}/>
		);
		wrapper.simulate('click');
		expect(onClick).toBeCalled();
	});
});
