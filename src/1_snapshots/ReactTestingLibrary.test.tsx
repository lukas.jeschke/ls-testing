import React from 'react';
import {fireEvent, render} from '@testing-library/react';
import {Harry} from "./components/Harry";

// https://testing-library.com/docs/intro

// The @testing-library family of packages helps you test UI components in a user-centric way.

// You want to write maintainable tests that give you high confidence that your components are working for your users.
// As a part of this goal, you want your tests to avoid including implementation details so refactors of your
// components (changes to implementation but not functionality) don't break your tests and slow you and your team down.

test('react testing library', () => {
	const onClick = jest.fn();
	const {getByText, container} = render(<Harry onClick={onClick}/>);

	// console.log(container.innerHTML);

	expect(container.getElementsByClassName("Harry")).toHaveLength(1);
	expect(container.getElementsByClassName("Harry")[0].firstElementChild?.classList).toContain('Vojta');
	expect(container.getElementsByClassName("Harry").item(0)?.firstElementChild?.classList).toContain('Vojta');
	expect(getByText("Kluk kodersky")).toBeInTheDocument();
	fireEvent.click(container.getElementsByClassName("Harry")[0]);
	expect(onClick).toBeCalled();
});
