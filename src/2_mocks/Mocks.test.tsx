import React from 'react';

jest.mock("../../contexts/SomeContext");

interface Tooltip {
	show: (elm: Element, elm_event: Event, opposite_direction?: boolean, border_elm?: Element) => void;
	hide: (elm: Element, elm_event: Event) => void;
	hide_all: () => void;
	init: () => void;
}

export interface Trans {
	translate(key: string, args?: string[]): string;
}

interface DI {
	getTrans: () => Trans;
	getTooltip: () => Tooltip;
	isMobile: () => boolean;
	isHarry: () => boolean;
}

// napr. z toho SomeContext
function useDI(): DI {
	return {} as DI;
}

export type Translations = {
	add: string;
	remove: string;
};

test('Mocks', () => {

	// type
	const translationsMock: Translations = {
		add: "Přidej tuto ligu do Mých lig!",
		remove: "Vyřaď tuto ligu z Mých lig!",
	};

	// mock
	const myMock = jest.fn();

	// class
	const TooltipMock = jest.fn<Tooltip, []>(
		() =>
			(({
				show: jest.fn(),
				hide: jest.fn(),
				hide_all: jest.fn(),
			} as unknown) as Tooltip),
	);

	// function
	(useDI as jest.Mock).mockImplementation(() => ({
		getTrans: () => ({ translate: (key: string) => key }),
		getTooltip: () => new TooltipMock(),
		isMobile: () => false,
	}));
});
