import React from "react";
import {Flag} from "./components/Flag";

interface LastMatchesTournamentCellProps {
	tournamentTemplateShortCode: string;
	tournamentUrl: string;
	tournamentTitle: string;
	flagId: number;
	flagName: string;
	isMobile: boolean;
}

export function LastMatchesTournamentCell({
	tournamentTemplateShortCode,
	tournamentUrl,
	tournamentTitle,
	flagId,
	flagName,
	isMobile,
}: LastMatchesTournamentCellProps) {
	if (isMobile) return null;

	return (
		<div className={"competition"}>
			<Flag id={flagId} name={flagName} extraStyles={"flag"} />
			<a href={tournamentUrl} onClick={event => event.stopPropagation()} title={tournamentTitle}>
				{tournamentTemplateShortCode}
			</a>
		</div>
	);
}
