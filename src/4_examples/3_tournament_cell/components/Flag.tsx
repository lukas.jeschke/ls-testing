import React from "react";

interface FlagProps {
	id: number;
	name: string;
	extraStyles?: string;
}

export function Flag({ id, name, extraStyles }: FlagProps) {
	return id ? <div title={name} className={`${extraStyles} fl_${id.toString()}`} /> : null;
}
