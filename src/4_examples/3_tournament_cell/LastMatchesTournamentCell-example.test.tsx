import { fireEvent, render } from "@testing-library/react";
import React from "react";
import { LastMatchesTournamentCell } from "./LastMatchesTournamentCell";

describe("LastMatchesTournamentCell", () => {
	it("should not render tournament | mobile", () => {
		const { container } = render(
			<LastMatchesTournamentCell
				tournamentTemplateShortCode={"SA"}
				tournamentTitle={"Serie A (Itálie)"}
				tournamentUrl={"/fotbal/italie/serie-a/"}
				flagId={98}
				flagName={"Itálie"}
				isMobile={true}
			/>,
		);

		expect(container.firstChild).toBeNull();
	});

	it("should render tournament | non mobile", () => {
		const { getByTitle, getByText, container } = render(
			<LastMatchesTournamentCell
				tournamentTemplateShortCode={"SA"}
				tournamentTitle={"Serie A (Itálie)"}
				tournamentUrl={"/fotbal/italie/serie-a/"}
				flagId={98}
				flagName={"Itálie"}
				isMobile={false}
			/>,
		);

		expect(container.getElementsByClassName("competition").item(0)).not.toBeNull();
		expect(container.getElementsByClassName("flag").item(0)).not.toBeNull();
		expect(container.getElementsByClassName("fl_98").item(0)).not.toBeNull();

		expect(getByTitle("Itálie")).toBeInTheDocument();
		expect(getByTitle("Serie A (Itálie)").getAttribute("href")).toBe("/fotbal/italie/serie-a/");
		expect(getByText("SA")).toBeInTheDocument();
	});

	it("handler click only for child element (not both child and parent)", () => {
		const parentOnCLick = jest.fn();
		const { getByTitle } = render(
			<div onClick={parentOnCLick}>
				<LastMatchesTournamentCell
					tournamentTemplateShortCode={"SA"}
					tournamentTitle={"Serie A (Itálie)"}
					tournamentUrl={"/fotbal/italie/serie-a/"}
					flagId={98}
					flagName={"Itálie"}
					isMobile={false}
				/>
			</div>,
		);

		fireEvent.click(getByTitle("Serie A (Itálie)"));
		expect(parentOnCLick).not.toBeCalled();
	});
});
