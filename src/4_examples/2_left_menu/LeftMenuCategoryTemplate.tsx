import * as React from "react";
import { useState } from "react";
import {TournamentTemplate} from "./components/TournamentTemplate";

export type Translations = {
	add: string;
	remove: string;
};

export interface CategoryMenuTemplateProps {
	template: TournamentTemplate;
	isFavourite: boolean;
	translations: Translations;
	handler: (key: string) => void;
	catUrl: string;
}

export const LeftMenuCategoryTemplate = ({
	template,
	isFavourite,
	translations,
	handler,
	catUrl,
}: CategoryMenuTemplateProps) => {
	const [isActive, setActive] = useState(isFavourite);
	const [isHover, setIsHover] = useState(false);
	const tooltip = isActive ? translations.remove : translations.add;

	return (
		<a
			href={!isHover ? catUrl + template.getUrl() : undefined}
			className={[
				"categoryListItem",
				"categoryListItemFS",
				template.getLeagueKey(),
				isHover ? "categoryListItem--active" : "",
			].join(" ")}
		>
			{template.getName()}
			<span
				className={["toggleElement", template.getLeagueKey(), isActive ? "toggleElementActive" : ""].join(" ")}
				title={tooltip}
				data-label-key={template.getLeagueKey()}
				onClick={event => {
					handler(template.getLeagueKey());
					event.stopPropagation();
					setActive(!isActive);
				}}
				onMouseEnter={() => setIsHover(true)}
				onMouseLeave={() => setIsHover(false)}
			/>
		</a>
	);
};
