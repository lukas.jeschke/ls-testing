import React from "react";
import {render, fireEvent} from "@testing-library/react";
import {LeftMenuCategoryTemplate} from "./LeftMenuCategoryTemplate";
import {Translations} from "../../2_mocks/Mocks.test";
import {TournamentTemplate} from "./components/TournamentTemplate";

describe("LeftMenuCategoryTemplate", () => {
	const translationsMock: Translations = {
		add: "Přidej tuto ligu do Mých lig!",
		remove: "Vyřaď tuto ligu z Mých lig!",
	};

	const TournamentTemplateMock = jest.fn<TournamentTemplate, []>(
		() =>
			(({
				getId: () => "345345",
				getCountryId: () => 234,
				getFlagId: () => 22,
				getUrl: () => "/anglie",
				getName: () => "anglie",
				getLeagueKey: () => "neconeco",
			} as unknown) as TournamentTemplate),
	);

	const handlerMock = jest.fn();


	it("should render", () => {
		const {container, getByText, getByTitle} = render(
			<LeftMenuCategoryTemplate
				template={new TournamentTemplateMock()}
				isFavourite={false}
				translations={translationsMock}
				handler={handlerMock}
				catUrl={"/anglie"}
			/>,
		);

		expect(container.getElementsByClassName("categoryListItem").item(0)).not.toBeNull();
		expect(container.getElementsByClassName("categoryListItemFS").item(0)).not.toBeNull();
		expect(container.getElementsByClassName("neconeco").item(0)).not.toBeNull();
		expect(container.getElementsByClassName("toggleElement").item(0)).not.toBeNull();

		expect(getByText("anglie")).toBeInTheDocument();
		expect(getByTitle("Přidej tuto ligu do Mých lig!")).toBeInTheDocument();
		expect(container.querySelector("a")).toBeInTheDocument();
		expect(container.querySelector("span")?.classList.contains("toggleElementActive")).toBe(false);
	});

	it("should handle onMouseEnter/onMouseLeave", () => {
		const {container} = render(
			<LeftMenuCategoryTemplate
				template={new TournamentTemplateMock()}
				isFavourite={false}
				translations={translationsMock}
				handler={handlerMock}
				catUrl={"/anglie"}
			/>,
		);

		const link = container.getElementsByClassName("categoryListItem").item(0)!;
		const myLeagueIcon = container.querySelector("span")!;

		fireEvent.mouseEnter(myLeagueIcon);
		expect(container.getElementsByClassName("categoryListItem--active").item(0)).not.toBeNull();
		expect(link.getAttribute("href")).toBe(null);

		fireEvent.mouseLeave(myLeagueIcon);
		expect(container.getElementsByClassName("categoryListItem--active").item(0)).toBeNull();
		expect(link.getAttribute("href")).toBe("/anglie/anglie");
	});

	it("should handle onMouseClick", () => {
		const parentOnCLick = jest.fn();

		const {container, getByText, getByTitle} = render(
			<div onClick={parentOnCLick}>
				<LeftMenuCategoryTemplate
					template={new TournamentTemplateMock()}
					isFavourite={false}
					translations={translationsMock}
					handler={handlerMock}
					catUrl={"/anglie"}
				/>
			</div>
		);

		const myLeagueIcon = container.querySelector("span")!;

		fireEvent.click(myLeagueIcon);
		expect(container.getElementsByClassName("toggleElementActive").item(0)).not.toBeNull();
		expect(handlerMock).toBeCalledWith("neconeco");
		expect(getByTitle("Vyřaď tuto ligu z Mých lig!")).toBeInTheDocument();
		expect(parentOnCLick).not.toBeCalled();
	});
});
