export interface TournamentTemplate {
	getId(): string;
	getCountryId(): number;
	getFlagId(): number;
	getUrl(): string;
	getName(): string;
	getLeagueKey(): string;
}
