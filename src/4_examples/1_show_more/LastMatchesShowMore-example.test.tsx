import { fireEvent, render } from "@testing-library/react";
import React from "react";
import { LastMatchesShowMore } from "./LastMatchesShowMore";

describe("LastMatchesShowMore", () => {
	const trans = { translate: (key: string) => key };
	const showMoreClick = jest.fn();

	it("should not show more header if not hasMore", () => {
		const { container } = render(
			<LastMatchesShowMore trans={trans} hasMore={false} showMoreOnClick={showMoreClick} />,
		);

		expect(container.firstChild).toBeNull();
	});

	it("should render show more if hasMore", () => {
		const { getByText, container } = render(
			<LastMatchesShowMore trans={trans} hasMore={true} showMoreOnClick={showMoreClick} />,
		);

		expect(container.getElementsByClassName("profileTable__row--last").item(0)).not.toBeNull();
		expect(container.getElementsByClassName("show-more-last-matches").item(0)).not.toBeNull();

		expect(getByText("TRANS_KEY")).toBeInTheDocument();
	});

	it("should dispatch action on clicking", () => {
		const { getByText } = render(
			<LastMatchesShowMore trans={trans} hasMore={true} showMoreOnClick={showMoreClick} />,
		);

		fireEvent.click(getByText("TRANS_KEY"));
		expect(showMoreClick).toBeCalled();
	});
});
