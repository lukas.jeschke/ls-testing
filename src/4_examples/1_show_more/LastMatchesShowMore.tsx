import React from "react";
import {Trans} from "./components/Trans";

interface LastMatchesShowMoreProps {
	showMoreOnClick: () => void;
	hasMore: boolean;
	trans: Trans;
}

export function LastMatchesShowMore({ trans, hasMore, showMoreOnClick }: LastMatchesShowMoreProps) {
	if (!hasMore) return null;

	return (
		<div className="profileTable__row profileTable__row--last show-more-last-matches">
			<a onClick={showMoreOnClick}>{trans.translate("TRANS_KEY")}</a>
		</div>
	);
}
