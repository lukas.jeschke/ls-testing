export interface Trans {
	translate(key: string, args?: string[]): string;
}
